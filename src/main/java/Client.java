public class Client implements Runnable {
    private final Bar bar;
    private final String name;

    Client(Bar bar, String name) {
        this.bar = bar;
        this.name = name;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                String drink = bar.takeDrink();
                getDrunk(drink);
            } catch (InterruptedException e) {

            }
        }

    }

    private void getDrunk(String drink) {
        try {
            System.out.println(name + " drinking " + drink);
            Thread.sleep(drink.length() * 1_000L);
        } catch (InterruptedException ex) {

        }
    }
}
