public class Main {
    public static void main(String[] args) {
        Bar bar = new Bar();
        Thread barman = new Thread(new Barman(bar));
        Thread[] clients = new Thread[5];

        for (int i = 0; i  < 5; i++) {
            clients[i] = new Thread(new Client(bar, "Client " + (i + 1)));
        }

        barman.start();

        for (int i = 0; i < 5; i++) {
            clients[i].start();
        }

        // TODO: Obsluzyc zakonczenie programu przez uzytkownika i zamkniecie dzialajacych watkow
    }
}
