import java.util.Random;

public class Barman implements Runnable {
    private static final String[] menu = {"Sex on the beach", "Margaritta", "Bloody Merry", "Malibou", "Mohito"};
    private final Bar bar;

    Barman(Bar bar) {
        this.bar = bar;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                bar.putDrink(getRandomDrink());
                Thread.sleep(1_000L); // Stawiaj drinka co 1 sekunde
            } catch (InterruptedException ex) {
                break;
            }
        }
    }

    private String getRandomDrink() {
        return menu[new Random().nextInt(5)];
    }
}
