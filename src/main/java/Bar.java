import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Bar {

    private List<String> drinks;

    Bar() {
        List<String> list = new ArrayList<>();
        drinks = Collections.synchronizedList(list);
    }

    synchronized String takeDrink() throws InterruptedException {
        while (drinks.isEmpty()) {
            wait();
        }
        String result = drinks.get(drinks.size() - 1); // wez ostatniego drinka z listy
        drinks.remove(drinks.size() - 1); // po wzieciu drinka usun go z listy dostepnych drinkow

        return result;
    }

    synchronized void putDrink(String drink) {
        drinks.add(drink);
        notifyAll();
    }
}
